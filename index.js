// for outputting data or text into the browser console
console.log('Hello World!')

// This is a singe-line comment

/* ctrl+shift+/ is for multi-line comments*/

let firstName = 'Rupert';
console.log(firstName);

// let variables are variable than can be re-assigned
let lastName = 'Ramos';
console.log(lastName);

let age = 10;
console.log(age);

firstName = 'Darien';
console.log(firstName)

// conts variables are variables that CANNOT be re-assigned
const colorOfTheSun = 'yellow';
console.log('The color of the sun is ' + colorOfTheSun);

/*
colorOfTheSun = 'red'
console.log(colorOfTheSun)*/

// Syntact of declaration
// declaring value
let variableName = 'value';
// re-assigning new value
variableName = 'new value';


// Strings - alphanumeric characters; denoted by single or double qoutation mark

let personName = 'Rupert Ramos';
console.log(personName)

// Numbers - include pos, neg  or number with decimals; no quotation marks

let personAge = 15;
console.log(personAge)

// Boolean - 2 logical values on; t or f, y or n

let hasCat = true;
console.log(hasCat)

// Array - multiple values; denoted by brackets 

let hobbies = ['Cooking', 'Playing Mobile Games', 'Watching Netflix'];
console.log(hobbies)

// Null - absence of a value; a place holder for future variable re-assignments

let sleep = null;
console.log(sleep)

// Object - other data or a set of info (array); denoted by curly braces

let personObj = {
	personName: 'Rupert Ramos',
	personAge: 15,
	hasCat: true,
	hobbies: ['Cooking', 'Playing Mobile Games', 'Watching Netflix']
}

console.log(personObj)

// to display single value of an object
console.log(personObj.personAge)

// todisplay single value of an array
console.log(hobbies[2])